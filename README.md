# HomebrewSoft Documentation
El objetivo de este repositorio es concentrar toda la documentación técnica requerida para el desarrollo de soluciones dentro de HomebrewSoft.
Si bien no toda la información está dentro de este repo, si se indicará de dónde conseguirla.

## Cursos Técnicos
- [Guia escrita](src/cursos_tecnicos/index.md)
- [Videos](https://drive.google.com/drive/folders/1QhqHWUBBcz0oDHKywvWpKzrTZubP3-sX?usp=sharing)

## Cursos Funcionales
- [Videos](https://drive.google.com/drive/folders/1zf5Udhe2Uf_hHuroIcTj4yQDzlBB3bWW?usp=sharing)

## Heramientas
- [instance-manager](https://gitlab.com/HomebrewSoft/tools/instance-manager)
- [mkmodule](https://gitlab.com/HomebrewSoft/tools/mkmodule)
- [sys_admin](https://gitlab.com/HomebrewSoft/tools/sys_admin)
- [vscode](https://gitlab.com/HomebrewSoft/tools/vscode)

## Imágenes de Docker
- [odoo](https://gitlab.com/HomebrewSoft/docker/odoo)
- [dockerhub](https://hub.docker.com/orgs/homebrewsoft)


## TODO's
- [ ] Cursos técnicos
  - [x] Linux
  - [x] Git
    - [ ] Tags utilizadas
    - [ ] Mecanismo de versionamiento
    - [ ] Mecanismo de revisión de código
    - [ ] Automatizar CI
  - [x] PostgreSQL
  - [x] Python
  - [ ] Docker
    - [ ] Volumenes
    - [ ] Docker file
    - [ ] Imagenes custom
    - [ ] Attach
    - [ ] Exec
  - [ ] Odoo
    - [ ] shell
    - [ ] odoo-terminal
    - [ ] Acciones de servidor
- [x] Cursos Funcionales
- [ ] Herramientas
  - [ ] Manejador de instancias
  - [ ] Creador de módulos
  - [ ] Server tools
- [ ] Imagenes Docker
  - [ ] Auto actualizar
- [ ] Servidor de desarrollo
- [ ] odoo.sh
  - [ ] Agregar sub-modulos
- [ ] Estandares utilizados
  - [ ] Configuración de VSCode
  - [ ] black
  - [ ] xml lintern
- [ ] Estandarizar canales de comunicación
