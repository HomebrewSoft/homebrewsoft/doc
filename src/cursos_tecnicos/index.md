# Cursos Técnicos

Los cursos listados en esta sección están pensados para que, al finalizarlos, el usuario sea capaz de desarrollar y modificar módulos para Odoo_ en su versión más reciente.

## Requisitos previos
- Familiaridad con la computación

## Recomendaciones
- Utilizar para todo el curso alguna distribución de Linux (De preferencia basada en Debian)


## Temas
- [Linux](linux.md)
- [Git](git.md)
- [PostgreSQL](postgres.md)
- [Python](python.md)
