# Python
Este curso se lleva a cabo siguiendo gran parte de la [guía oficial de Python](https://docs.python.org/3.7/tutorial/) y su propósito es aprender lo necesario para el desarrollo de módulos en el mundo de Odoo, por lo que no se tocan temas relacionados a la entrada y salida de datos mediante consola o el uso de archivos u otros artefactos ajenos.

## Requisitos previos
- Python 3.7 o superior
- Editor de textos

## Temario
- [1. Whetting Your Appetite](https://docs.python.org/3.7/tutorial/appetite.html#whetting-your-appetite)
- [2.1.2. Interactive Mode](https://docs.python.org/3.7/tutorial/interpreter.html#interactive-mode)
- [3. An Informal Introduction to Python](https://docs.python.org/3.7/tutorial/introduction.html#an-informal-introduction-to-python)
    - [3.1.1. Numbers](https://docs.python.org/3.7/tutorial/introduction.html#numbers)
    - [3.1.2. Strings](https://docs.python.org/3.7/tutorial/introduction.html#strings)
    - [3.1.3. Lists](https://docs.python.org/3.7/tutorial/introduction.html#lists)
    - [3.2. First Steps Towards Programming](https://docs.python.org/3.7/tutorial/introduction.html#first-steps-towards-programming)
- [4.1. if Statements](https://docs.python.org/3.7/tutorial/controlflow.html#if-statements)
    - [4.2. for Statements](https://docs.python.org/3.7/tutorial/controlflow.html#for-statements)
    - [4.3. The range() Function](https://docs.python.org/3.7/tutorial/controlflow.html#the-range-function)
    - [4.4. break and continue Statements, and else Clauses on Loops](https://docs.python.org/3.7/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops)
    - [4.5. pass Statements](https://docs.python.org/3.7/tutorial/controlflow.html#pass-statements)
    - [4.6. Defining Functions](https://docs.python.org/3.7/tutorial/controlflow.html#defining-functions)
    - [4.7.1. Default Argument Values](https://docs.python.org/3.7/tutorial/controlflow.html#default-argument-values)
    - [4.7.2. Keyword Arguments](https://docs.python.org/3.7/tutorial/controlflow.html#keyword-arguments)
    - [4.7.5. Lambda Expressions](https://docs.python.org/3.7/tutorial/controlflow.html#lambda-expressions)
    - [4.8. Intermezzo: Coding Style](https://docs.python.org/3.7/tutorial/controlflow.html#intermezzo-coding-style)
- [5.1. More on Lists](https://docs.python.org/3.7/tutorial/datastructures.html#more-on-lists)
    - [5.1.1. Using Lists as Stacks](https://docs.python.org/3.7/tutorial/datastructures.html#using-lists-as-stacks)
    - [5.1.2. Using Lists as Queues](https://docs.python.org/3.7/tutorial/datastructures.html#using-lists-as-queues)
    - [5.1.3. List Comprehensions](https://docs.python.org/3.7/tutorial/datastructures.html#list-comprehensions)
    - [5.3. Tuples and Sequences](https://docs.python.org/3.7/tutorial/datastructures.html#tuples-and-sequences)
    - [5.4. Sets](https://docs.python.org/3.7/tutorial/datastructures.html#sets)
    - [5.5. Dictionaries](https://docs.python.org/3.7/tutorial/datastructures.html#dictionaries)
    - [5.6. Looping Techniques](https://docs.python.org/3.7/tutorial/datastructures.html#looping-techniques)
    - [5.7. More on Conditions](https://docs.python.org/3.7/tutorial/datastructures.html#more-on-conditions)
- [6. Modules](https://docs.python.org/3.7/tutorial/modules.html)
    - [6.1. More on Modules](https://docs.python.org/3.7/tutorial/modules.html#more-on-modules)
    - [6.1.3. “Compiled” Python files](https://docs.python.org/3.7/tutorial/modules.html#compiled-python-files)
    - [6.4. Packages](https://docs.python.org/3.7/tutorial/modules.html#packages)
    - [6.4.1. Importing * From a Package](https://docs.python.org/3.7/tutorial/modules.html#importing-from-a-package)
- [8.1. Syntax Errors](https://docs.python.org/3.7/tutorial/errors.html#syntax-errors)
    - [8.2. Exceptions](https://docs.python.org/3.7/tutorial/errors.html#exceptions)
    - [8.3. Handling Exceptions](https://docs.python.org/3.7/tutorial/errors.html#handling-exceptions)
    - [8.4. Raising Exceptions](https://docs.python.org/3.7/tutorial/errors.html#raising-exceptions)
    - [8.6. Defining Clean-up Actions](https://docs.python.org/3.7/tutorial/errors.html#defining-clean-up-actions)
- [9. Classes](https://docs.python.org/3.7/tutorial/classes.html#classes)
    - [9.3.1. Class Definition Syntax](https://docs.python.org/3.7/tutorial/classes.html#class-definition-syntax)
    - [9.3.2. Class Objects](https://docs.python.org/3.7/tutorial/classes.html#class-objects)
    - [9.3.3. Instance Objects](https://docs.python.org/3.7/tutorial/classes.html#instance-objects)
    - [9.3.4. Method Objects](https://docs.python.org/3.7/tutorial/classes.html#method-objects)
    - [9.3.5. Class and Instance Variables](https://docs.python.org/3.7/tutorial/classes.html#class-and-instance-variables)
    - [9.5. Inheritance](https://docs.python.org/3.7/tutorial/classes.html#inheritance)

Apéndices
---------
- https://docs.python.org/3.7/tutorial/
